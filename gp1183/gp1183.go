package gp1183

import (
	"github.com/stianeikeland/go-rpio/v4"
	"time"
)

// Hat pin mapping
const (
	pinD0 = 16
	pinD1 = 17
	pinD2 = 18
	pinD3 = 19
	pinD4 = 20
	pinD5 = 21
	pinD6 = 22
	pinD7 = 23
	pinWR = 24
)

const (
	FontAmerica      = 0
	FontFrance       = 1
	FontGermany      = 2
	FontEngland      = 3
	FontDenmark      = 4
	FontSweden       = 5
	FontItaly        = 6
	FontSpain        = 7
	FontJapan        = 8
	FontNorway       = 9
	FontDemark2      = 10
	FontSpain2       = 11
	FontLatinAmerica = 12
	FontKorea        = 13
)

const (
	CharPC437    = 0
	CharKatakana = 1
	CharPC850    = 2
	CharPC860    = 3
	CharPC863    = 4
	CharPC865    = 5
	CharWPC1252  = 16
	CharPC866    = 17
	CharPC852    = 18
	CharPC858    = 19
	CharLatin2   = CharPC852
)

const (
	ModeNormal = 0
	ModeOR     = 1
	ModeAND    = 2
	ModeXOR    = 3
)

const (
	Glyph5x7 = 5
	Glyph7x8 = 8
)

const (
	cmdDelay = 50 * time.Microsecond
)

type GP1183 struct {
	dataPins []rpio.Pin
	wr       rpio.Pin
}

func Make() (*GP1183, error) {
	err := rpio.Open()

	pins := []int{pinD0, pinD1, pinD2, pinD3, pinD4, pinD5, pinD6, pinD7}

	if err != nil {
		return nil, err
	}

	ret := GP1183{}
	ret.dataPins = make([]rpio.Pin, 8)

	for i := range pins {
		ret.dataPins[i] = rpio.Pin(pins[i])
		ret.dataPins[i].Mode(rpio.Output)
		ret.dataPins[i].Write(rpio.Low)
	}

	ret.wr = rpio.Pin(pinWR)
	ret.wr.Mode(rpio.Output)
	ret.wr.Write(rpio.High)

	ret.Init()

	return &ret, nil
}

func (gp *GP1183) Init() {
	gp.WriteBytes([]byte{0x1B, 0x40})
	time.Sleep(10 * time.Millisecond)
	gp.Clear()
}

func (gp *GP1183) WriteMode(mode byte) {
	gp.WriteBytes([]byte{0x1F, 0x77, mode})
}

func (gp *GP1183) Brightness(value byte) {
	gp.WriteBytes([]byte{0x1F, 0x58, value})
}

func (gp *GP1183) Sleep(time byte) {
	gp.WriteBytes([]byte{0x1F, 0x28, 0x61, 0x01, time})
}

func (gp *GP1183) Scroll(shift, repeat int, speed byte) {
	shiftHigh, shiftLow := split(shift)
	repeatHigh, repeatLow := split(repeat)

	gp.WriteBytes([]byte{0x1F, 0x28, 0x61, 0x10, shiftLow, shiftHigh, repeatLow, repeatHigh, speed})
}

func (gp *GP1183) Blink(pattern byte, t1, t2 byte, repeat byte) {
	gp.WriteBytes([]byte{0x1F, 0x28, 0x61, 0x11, pattern, t1, t2, repeat})
}

func (gp *GP1183) ScreenSaver(mode byte) {
	gp.WriteBytes([]byte{0x1F, 0x28, 0x61, 0x40, mode})
}

func (gp *GP1183) Bitmap(x, y int, data []byte) {
	xHigh, xLow := split(x)
	yHigh, yLow := split(y)

	gp.WriteBytes([]byte{0x1F, 0x28, 0x66, 0x11, xLow, xHigh, yLow, yHigh, 0x01})
	gp.WriteBytes(data)
}

func (gp *GP1183) CharWidth(size byte) {
	gp.WriteBytes([]byte{0x1F, 0x28, 0x67, 0x03, size})
}

func (gp *GP1183) CharSize(x, y byte) {
	gp.WriteBytes([]byte{0x1F, 0x28, 0x67, 0x40, x, y})
}

func (gp *GP1183) DefineGlyph(code byte, size byte, data []byte) {
	cmd := []byte{0x1B, 0x26, 0x01, code, code, size}
	cmd = append(cmd, data...)
	gp.WriteBytes(cmd)
}

func (gp *GP1183) DeleteGlyph(code byte) {
	gp.WriteBytes([]byte{0x1B, 0x3F, 0x01, code})
}

func (gp *GP1183) Home() {
	gp.writeByte(0x0B)
	time.Sleep(cmdDelay)
}

func (gp *GP1183) RowHome() {
	gp.writeByte(0x0D)
	time.Sleep(cmdDelay)
}

func (gp *GP1183) Cursor(x, y int) {
	xHigh, xLow := split(x)
	yHigh, yLow := split(y)

	gp.WriteBytes([]byte{0x1F, 0x24, xLow, xHigh, yLow, yHigh})
	time.Sleep(cmdDelay)
}

func (gp *GP1183) CustomChars(enable bool) {
	b := byte(0)
	if enable {
		b = 1
	}

	gp.WriteBytes([]byte{0x1B, 0x25, b})
}

func (gp *GP1183) Overwrite() {
	gp.WriteBytes([]byte{0x1F, 0x01})
}

func (gp *GP1183) VerticalScroll() {
	gp.WriteBytes([]byte{0x1F, 0x02})
}

func (gp *GP1183) HorizontalScroll() {
	gp.WriteBytes([]byte{0x1F, 0x03})
}

func (gp *GP1183) ScrollSpeed(speed byte) {
	gp.WriteBytes([]byte{0x1F, 0x73, speed})
}

func (gp *GP1183) ReverseDisplay(enabled bool) {
	cmd := byte(0)
	if enabled {
		cmd = 1
	}
	gp.WriteBytes([]byte{0x1F, 0x72, cmd})
}

func (gp *GP1183) PrevChar() {
	gp.writeByte(0x08)
	time.Sleep(cmdDelay)
}

func (gp *GP1183) NextChar() {
	gp.writeByte(0x09)
	time.Sleep(cmdDelay)
}

func (gp *GP1183) NextRow() {
	gp.writeByte(0x0A)
	time.Sleep(cmdDelay)
}

func (gp *GP1183) Charset(set byte) {
	gp.WriteBytes([]byte{0x1B, 0x74, set})
	time.Sleep(cmdDelay)
}

func (gp *GP1183) Font(font byte) {
	gp.WriteBytes([]byte{0x1B, 0x52, font})
	time.Sleep(cmdDelay)
}

func (gp *GP1183) Clear() {
	gp.WriteBytes([]byte{0x0C})
	time.Sleep(7 * time.Millisecond)
}

func (gp *GP1183) writeByte(data byte) {
	gp.wr.Write(rpio.Low)

	for i := 0; i < 8; i++ {
		if (data>>i)&1 == 1 {
			gp.dataPins[i].Write(rpio.High)
		} else {
			gp.dataPins[i].Write(rpio.Low)
		}
	}
	time.Sleep(2 * time.Microsecond)
	gp.wr.Write(rpio.High)
	time.Sleep(50 * time.Microsecond)
}

func (gp *GP1183) WriteBytes(data []byte) {
	for _, b := range data {
		gp.writeByte(b)
	}
}

func split(value int) (high, low byte) {
	low = byte(value & 0xFF)
	high = byte((value >> 8) & 0xFF)
	return
}
