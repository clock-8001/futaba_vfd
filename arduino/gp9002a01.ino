// Test code for the Futaba GP8002A01 VFD module

#define D0 12
#define D1 11
#define D2 10
#define D3 9
#define D4 8
#define D5 7
#define D6 6
#define D7 5

#define WR A0
#define RD A1
#define CS A2
#define CD A3

// Commands
#define DisplayOff      0x00
#define Screen1On       0x01
#define Screen2On       0x02
#define AddrIncrement   0x04
#define AddrHold        0x05
#define ClearScreen     0x06
#define PowerControl    0x07
#define WriteData       0x08
#define ReadData        0x09
#define Screen1LowAddr  0x0A
#define Screen1HighAddr 0x0B
#define Screen2LowAddr  0x0C
#define Screen2HighAddr 0x0D
#define DataLowAddr     0x0E
#define DataHighAddr    0x0F
#define DisplayOR       0x10
#define DisplayXOR      0x11
#define DisplayAND      0x12
#define LuminanceAdj    0x13
#define DisplayMode     0x14
#define IntMode         0x15
#define DisplayChar     0x20
#define CharLocation    0x21
#define CharSize        0x22
#define CharBrightness  0x24

uint8_t dataPins[] = {D0, D1, D2, D3, D4, D5, D6, D7};

void setup() {
  for (uint8_t i = 0; i < 8; i++) {
    pinMode(dataPins[i], OUTPUT);
    digitalWrite(dataPins[i], LOW);
  }

  pinMode(CS, OUTPUT);
  digitalWrite(CS, HIGH);

  pinMode(WR, OUTPUT);
  digitalWrite(WR, HIGH);

  pinMode(RD, OUTPUT);
  digitalWrite(RD, HIGH);

  pinMode(CD, OUTPUT);
  digitalWrite(CD, HIGH);

  Serial.begin(38400);
}

void loop() {
  Serial.println("Looping:");
  // Display mode
  writeCmd(DisplayMode);
  writeData(0x10);

  // Clear screen
  writeCmd(ClearScreen);

  // Display 1 on
  writeCmd(Screen1On);

  // Set display 1 start address low
  writeCmd(Screen1LowAddr);
  writeData(0x10);

  // Set display 2 start address high
  writeCmd(Screen1HighAddr);
  writeData(0x00);

  // Write data in
  writeCmd(WriteData);
  for (uint16_t i = 0; i < 128*8; i ++) {
    writeData(0x10);
  }

  delay(10000);
 }

void writeData(uint8_t data) {
  digitalWrite(CD, LOW);
  writeByte(data);
}

void writeCmd(uint8_t cmd ) {
  digitalWrite(CD, HIGH);
  writeByte(cmd);
}

void writeBytes(uint8_t bytes[], uint8_t len) {

  for (uint8_t i = 0; i < len; i++) {
    writeByte(bytes[i]);
  }
}

void writeByte(uint8_t data) {
  digitalWrite(CS, LOW);
  delayMicroseconds(100);
  digitalWrite(WR, LOW);

  for (uint8_t i = 0; i < 8; i++) {
    if ((data >> i) & 1 == 1) {
      digitalWrite(dataPins[i], HIGH);
    } else {
      digitalWrite(dataPins[i], LOW);
    }
  }

  digitalWrite(WR, HIGH);
  delayMicroseconds(100);
  digitalWrite(CS, HIGH);
}
