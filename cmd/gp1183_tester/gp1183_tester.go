package main

import (
	"time"

	"gitlab.com/clock-8001/futaba_vfd/gp1183"
)

func main() {
	time.Sleep(10 * time.Millisecond)

	arrowDown := []byte{0x00, 0x08, 0x0c, 0x7e, 0x7e, 0x0c, 0x08, 0x00}

	gp, err := gp1183.Make()
	if err != nil {
		panic(err)
	}
	for {
		gp.Home()
		gp.WriteBytes([]byte("Hello World!"))

		gp.NextRow()
		gp.RowHome()
		text := time.Now().Local().Format("15:04:05")
		gp.Bitmap(8, 1, arrowDown)

		gp.NextChar()
		gp.NextChar()

		gp.WriteBytes([]byte(text))

		time.Sleep(30 * time.Millisecond)
	}
}
