package main

import (
	"fmt"
	"gitlab.com/clock-8001/futaba_vfd/gp9002"
	"time"
)

func main() {
	img := []byte{ // 'icon256', 64x128px
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x1f, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0x80, 0x00, 0x00,
		0x00, 0x00, 0x07, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x1f, 0xff, 0xff, 0xf8, 0x00, 0x00,
		0x00, 0x00, 0x7f, 0xe0, 0x07, 0xfe, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0xff, 0x00, 0x00,
		0x00, 0x01, 0xfc, 0x00, 0x00, 0x3f, 0x80, 0x00, 0x00, 0x03, 0xf0, 0x00, 0x00, 0x0f, 0xc0, 0x00,
		0x00, 0x07, 0xe0, 0x01, 0x80, 0x07, 0xe0, 0x00, 0x00, 0x0f, 0x80, 0x03, 0xc0, 0x01, 0xf0, 0x00,
		0x00, 0x1f, 0x00, 0x03, 0xc0, 0x00, 0xf8, 0x00, 0x00, 0x3e, 0x00, 0x03, 0xc0, 0x00, 0x7c, 0x00,
		0x00, 0x3e, 0x00, 0x03, 0xc0, 0x00, 0x7c, 0x00, 0x00, 0x7c, 0x00, 0x03, 0xc0, 0x00, 0x3e, 0x00,
		0x00, 0x78, 0x00, 0x03, 0xc0, 0x00, 0x1e, 0x00, 0x00, 0xf8, 0x00, 0x03, 0xc0, 0x00, 0x1f, 0x00,
		0x00, 0xf0, 0x00, 0x03, 0xc0, 0x00, 0x0f, 0x00, 0x01, 0xf0, 0x00, 0x03, 0xc0, 0x00, 0x0f, 0x80,
		0x01, 0xe0, 0x00, 0x03, 0xc0, 0x00, 0x07, 0x80, 0x01, 0xe0, 0x00, 0x03, 0xc0, 0x00, 0x07, 0x80,
		0x01, 0xe0, 0x00, 0x03, 0xc0, 0x00, 0x07, 0x80, 0x03, 0xc0, 0x00, 0x03, 0xc0, 0x00, 0x03, 0xc0,
		0x03, 0xc0, 0x00, 0x03, 0xc0, 0x00, 0x03, 0xc0, 0x03, 0xc0, 0x00, 0x03, 0xc0, 0x00, 0x03, 0xc0,
		0x03, 0xc0, 0x00, 0x03, 0xc0, 0x00, 0x03, 0xc0, 0x03, 0xc0, 0x00, 0x03, 0xc0, 0x00, 0x03, 0xc0,
		0x03, 0xc0, 0x00, 0x03, 0xff, 0xf8, 0x03, 0xc0, 0x03, 0xc0, 0x00, 0x03, 0xff, 0xfc, 0x03, 0xc0,
		0x03, 0xc0, 0x00, 0x03, 0xff, 0xfc, 0x03, 0xc0, 0x03, 0xc0, 0x00, 0x01, 0xff, 0xf8, 0x03, 0xc0,
		0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x01, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0x80,
		0x01, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0x80, 0x01, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0x80,
		0x01, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x80, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00,
		0x00, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x00,
		0x00, 0x7c, 0x00, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x00, 0x3e, 0x00, 0x00, 0x00, 0x00, 0x7c, 0x00,
		0x00, 0x3e, 0x00, 0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x00,
		0x00, 0x0f, 0x80, 0x00, 0x00, 0x01, 0xf0, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x07, 0xe0, 0x00,
		0x00, 0x03, 0xf0, 0x00, 0x00, 0x0f, 0xc0, 0x00, 0x00, 0x01, 0xfc, 0x00, 0x00, 0x3f, 0x80, 0x00,
		0x00, 0x00, 0xff, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xe0, 0x07, 0xfe, 0x00, 0x00,
		0x00, 0x00, 0x1f, 0xff, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x07, 0xff, 0xff, 0xe0, 0x00, 0x00,
		0x00, 0x00, 0x01, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0xf8, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

	arrowDown := []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0xc0, 0x00, 0xe0, 0x00, 0xf0, 0x7f, 0xf8, 0x7f, 0xfc, 0x7f, 0xfc, 0x7f, 0xf8, 0x00, 0xf0, 0x00, 0xe0, 0x00, 0xc0, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00}
	arrowUp := []byte{0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x06, 0x00, 0x0e, 0x00, 0x1e, 0x00, 0x3f, 0xfc, 0x7f, 0xfc, 0x7f, 0xfc, 0x3f, 0xfc, 0x1e, 0x00, 0x0e, 0x00, 0x06, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00}
	paused := []byte{0x00, 0x00, 0x00, 0x00, 0x7f, 0xfc, 0x7f, 0xfc, 0x7f, 0xfc, 0x7f, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xfc, 0x7f, 0xfc, 0x7f, 0xfc, 0x7f, 0xfc, 0x00, 0x00, 0x00, 0x00}
	playPause := []byte{0x00, 0x00, 0x00, 0x00, 0x7f, 0xfc, 0x3f, 0xf8, 0x1f, 0xf0, 0x0f, 0xe0, 0x07, 0xc0, 0x03, 0x80, 0x01, 0x00, 0x00, 0x00, 0x7f, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xfc, 0x00, 0x00, 0x00, 0x00}
	play := []byte{0x00, 0x00, 0x00, 0x00, 0x7f, 0xfc, 0x3f, 0xf8, 0x3f, 0xf8, 0x1f, 0xf0, 0x0f, 0xe0, 0x0f, 0xe0, 0x07, 0xc0, 0x03, 0x80, 0x03, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	repeat := []byte{0x00, 0x00, 0x00, 0x00, 0x0c, 0x30, 0x1e, 0x30, 0x3f, 0x30, 0x0c, 0x30, 0x0c, 0x30, 0x0c, 0x30, 0x0c, 0x30, 0x0c, 0x30, 0x0c, 0x30, 0x0c, 0xfc, 0x0c, 0x78, 0x0c, 0x30, 0x00, 0x00, 0x00, 0x00}
	target := []byte{0x00, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x1f, 0xf8, 0x0f, 0xf0, 0x07, 0xe0, 0x03, 0xc0, 0x01, 0x80, 0x3f, 0xfc, 0x00, 0x00, 0x00, 0x00}
	record := []byte{0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x07, 0xe0, 0x0f, 0xf0, 0x1f, 0xf8, 0x1f, 0xf8, 0x3f, 0xfc, 0x3f, 0xfc, 0x1f, 0xf8, 0x1f, 0xf8, 0x0f, 0xf0, 0x07, 0xe0, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00}
	overTime := []byte{0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x3f, 0xfc, 0x3f, 0xfc, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00}

	fmt.Printf("Testing the display...\n")

	gp, err := gp9002.Make()

	if err != nil {
		panic(err)
	}

	gp.Init()

	gp.WriteCommand(gp9002.WriteData)
	gp.WriteBytes(img)

	gp.Init()

	time.Sleep(100 * time.Microsecond)

	symbol := 0

	icons := []*[]byte{&arrowDown, &arrowUp, &paused, &playPause, &play, &repeat, &target, &record, &overTime}

	icon_index := 0
	bitmap := &arrowDown

	for {
		fmt.Printf(".")

		gp.CharLocation(5, 3)
		gp.CharSize(0, 0)
		gp.WriteChars("Time of Day")

		gp.CharLocation(35, 16)
		gp.CharSize(1, 1)
		text := time.Now().Local().Format("15:04:05")
		gp.WriteChars(text)

		for i, b := range arrowUp {
			y := 2 + (i % 2)  // bytes
			x := 10 + (i / 2) // pixels
			addr := uint16(y + (x * 8))
			gp.DataLocation(addr)
			gp.WriteBytes([]byte{b})
		}

		gp.CharLocation(5, 35)
		gp.CharSize(0, 0)
		gp.WriteChars("Timer 1")

		gp.CharLocation(35, 48)
		gp.CharSize(1, 1)
		gp.WriteChars("00:12:34")

		if symbol > 50 {
			icon_index = (icon_index + 1) % len(icons)
			bitmap = icons[icon_index]
			symbol = 0
		}

		for i, b := range *bitmap {
			y := 6 + (i % 2)  // bytes
			x := 10 + (i / 2) // pixels
			addr := uint16(y + (x * 8))
			gp.DataLocation(addr)
			gp.WriteBytes([]byte{b})
		}

		symbol++
		time.Sleep(30 * time.Millisecond)
	}
}
